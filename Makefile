BR2_EXTERNAL_DIR := $(shell readlink -f .)
BUILDROOT_DIR := $(BR2_EXTERNAL_DIR)/buildroot
OUTPUT_DIR := $(BR2_EXTERNAL_DIR)/output

TARGETS = \
  check-check-package \
  update-reference-log \
  check-package-reference-log \
  check-package-python-version \
  unit-tests \
  unit-tests-style

.PHONY: default
default: check-check-package

ifeq (,$(wildcard /INSIDE_DOCKER))

ALL_TARGETS = $(TARGETS) submodules checkout
.PHONY: $(ALL_TARGETS)
$(ALL_TARGETS):
	./docker-run make $@

else # ifneq (,$(wildcard /INSIDE_DOCKER))

.PHONY: submodules
submodules: $(BUILDROOT_DIR)/README
$(BUILDROOT_DIR)/README:
	git submodule init
	git submodule update

.PHONY: checkout
checkout: $(OUTPUT_DIR)/Makefile
$(OUTPUT_DIR)/Makefile: submodules
	make -C $(BUILDROOT_DIR) BR2_EXTERNAL=$(BR2_EXTERNAL_DIR) O=$(OUTPUT_DIR) defconfig

.PHONY: $(TARGETS)
$(TARGETS): checkout
	make -C $(OUTPUT_DIR) $@

endif # ifneq (,$(wildcard /INSIDE_DOCKER))

.PHONY: clean
clean:
	rm -rf $(OUTPUT_DIR)

.PHONY: distclean
distclean: clean
	rm -rf $(BUILDROOT_DIR)
